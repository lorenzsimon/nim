<div align="center">
  <img src="assets/img/logo.png" alt="Logo" width="200">

  ### A Java GUI for playing the strategic game "Nim" against an intelligent machine.
</div>

<br>
<br>
<br>

## About The Project

This project uses the Java Swing library to build an interactive strategic game. 

> **Nim** is a mathematical game of strategy in which two players take turns removing (or "nimming") objects from distinct heaps or piles. On each turn, a player must remove at least one object, and may remove any number of objects provided they all come from the same heap or pile. Depending on the version being played, the goal of the game is either to avoid taking the last object or to take the last object. -- <cite>[Wikipedia](https://en.wikipedia.org/wiki/Nim)</cite>

The user plays against a machine that applies a certain strategy to win.

> The key to the theory of the game is the binary digital sum of the heap sizes, that is, the sum (in binary) neglecting all carries from one digit to another. This operation is also known as "bitwise xor" or "vector addition over GF(2)" (bitwise addition modulo 2). Within combinatorial game theory it is usually called the nim-sum, as it will be called here. The nim-sum of x and y is written x ⊕ y to distinguish it from the ordinary sum, x + y.
>
> In normal play, the winning strategy is to finish every move with a nim-sum of 0. This is always possible if the nim-sum is not zero before the move. If the nim-sum is zero, then the next player will lose if the other player does not make a mistake. To find out which move to make, let X be the nim-sum of all the heap sizes. Find a heap where the nim-sum of X and heap-size is less than the heap-size - the winning strategy is to play in such a heap, reducing that heap to the nim-sum of its original size with X.  -- <cite>[Wikipedia](https://en.wikipedia.org/wiki/Nim)</cite>

There are two versions of the game. In the normal version, the player who removes the last objects wins. In the misère version, the player who removes the last object looses.


## Getting Started

In order to use this software and play the game you have to download the [released](https://gitlab.com/lorenzsimon/nim/-/releases) jar file and move it to your application directory. You can then start it by opening the jar file.



## Usage

Click on "New" to start a new game. You can specify how many sticks you want to have in each row. In the example below we get five rows with 1, 2, 4, 8 and 16 sticks.

<img src="assets/img/screenshot1.png" alt="drawing" width="400"/>

To remove sticks from a row you have click on a stick on the board. All sticks to the right are removed. In the example below we remove 6 sticks from the fourth row.

<img src="assets/img/screenshot2.png" alt="drawing" width="800"/>


## License

Distributed under the GNU GLP License. See `LICENSE` for more information.

