package nim;

import nim.view.NimGUI;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;

import java.awt.Dimension;

/**
 * GNim is the graphic version of the nim game. This program provides a
 * graphical interface for the player to interact with the game.
 * <p>
 * This class is responsible for starting a new GUI that enables the user to
 * interact with the GNim program.
 */
public final class GNim {

    /**
     * Suppressed default constructor.
     */
    private GNim() {
    }

    /**
     * The starting point of the program where the GUI is created.
     *
     * @param args Optional arguments for the program start.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {

            /**
             * {@inheritDoc}
             */
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    /**
     * Creates the GUI and displays the frame in a appropriate size.
     */
    private static void createAndShowGUI() {
        JFrame gui = new NimGUI();
        gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        gui.pack();
        gui.setMinimumSize(new Dimension(300, 200));
        gui.setVisible(true);
    }
}
