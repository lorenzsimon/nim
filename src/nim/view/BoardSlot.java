package nim.view;

import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.event.MouseListener;

import static java.lang.Double.min;

/**
 * The BoardSlot is responsible for displaying one slot on a play board.
 * <p>
 * A board slot can contain a stick, or be empty. It also can displayed as
 * selected which is indicated by a blue background instead of a green one.
 */
public class BoardSlot extends JPanel {

    /**
     * The serial version UID of this class.
     */
    private static final long serialVersionUID = 4279818684941777849L;

    /**
     * The default background color of a slot.
     * The color is global and static for better efficiency of the
     * {@link #paintComponent(Graphics)} method.
     */
    private static final Color DEFAULT_BACKGROUND = new Color(66, 153, 65);

    /**
     * The background color of a selected slot.
     */
    private static final Color SELECTED_BACKGROUND = new Color(47, 123, 207);

    /**
     * The row of the corresponding play board this slot is located in.
     */
    private int row;

    /**
     * The column of the corresponding play board this slot is located in.
     */
    private int col;

    /**
     * A flag that indicates whether this slot is selected.
     */
    private boolean selected;

    /**
     * A flag that indicates whether the slot contains a stick.
     */
    private boolean stickcontaining;

    /**
     * Constructor for a new BoardSlot located in the provided row {@code row}
     * and column {@code col} and with the provided listener added the its
     * listener list.
     * <p>
     * A new slot doesn't contain a stick and is not selected.
     *
     * @param row      The row this slot is located in.
     * @param col      The column this slot is located in.
     * @param listener A {@link MouseListener} for this slot.
     */
    public BoardSlot(int row, int col, MouseListener listener) {
        setPreferredSize(new Dimension(40, 100));

        this.row = row;
        this.col = col;

        selected = false;
        stickcontaining = false;

        addMouseListener(listener);
    }

    /**
     * Gets the row this slot is located in.
     *
     * @return The row this slot is located in.
     */
    public int getRow() {
        return row;
    }

    /**
     * Gets the column this slot is located in.
     *
     * @return The column this slot is located in.
     */
    public int getCol() {
        return col;
    }

    /**
     * Checks if this slot contains a stick.
     *
     * @return {@code True} if this slot contains a stick or {@code false} if it
     * doesn't.
     */
    public boolean containsStick() {
        return stickcontaining;
    }

    /**
     * Determines whether this slot contains a stick.
     *
     * @param stickcontaining The flag that decides if this slot contains a
     *                        stick or not.
     */
    public void setContainsStick(boolean stickcontaining) {
        this.stickcontaining = stickcontaining;
        repaint();
    }

    /**
     * Checks if this slot is selected.
     *
     * @return {@code True} if this slot is selected or {@code false} if not.
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Determines if this slot is selected.
     *
     * @param selected The flag that decides if this slot is selected or not.
     */
    public void setIsSelected(boolean selected) {
        this.selected = selected;
        repaint();
    }

    /**
     * Paints this board slot to the screen. Dependent on the flags set it
     * paints a green or blue background with or without a matchstick on it.
     *
     * @param g The graphics context with which to paint the board slot.
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        // The current height (and width) of this slot.
        final int slotHeight = getSize().height;
        final int slotWidth = getSize().width;

        // First paint the background blue if selected or green if not.
        if (selected) {
            g2.setColor(SELECTED_BACKGROUND);
            g2.fillRect(0, 0, slotWidth, slotHeight);
        } else {
            g2.setColor(DEFAULT_BACKGROUND);
            g2.fillRect(0, 0, slotWidth, slotHeight);
        }

        // Paint a stick on top if this slot contains a stick.
        if (stickcontaining) {

            // The height of the wood part takes up 70% of the slot height.
            final int woodHeight = (int) (slotHeight * 0.7);

            /*
            The width of the wood part takes up either 70% of the slot width or
            12% of the wood part height, whichever is smaller.
            This ensures that we stay within this slot.
             */
            final int woodWidth = (int) min((woodHeight * 0.12),
                    (slotWidth * 0.7));

            // Paint in the center of this slot.
            final int xPosRec = (slotWidth / 2) - (woodWidth / 2);
            final int yPosRec = (slotHeight / 2) - (woodHeight / 2);

            g2.setColor(Color.YELLOW);
            g2.fillRect(xPosRec, yPosRec, woodWidth, woodHeight);

            /*
            The diameter of the sticks head part takes up either 30% of the wood
            part height or the entire width of the slot, whichever is smaller.
             */
            final int headDiameter = (int) min((woodHeight * 0.3), slotWidth);

            // Paint on top of the wood part.
            final int xPosOval = (slotWidth / 2) - (headDiameter / 2);
            final int yPosOval = yPosRec - (headDiameter / 2);

            g2.setColor(Color.RED);
            g2.fillOval(xPosOval, yPosOval, headDiameter, headDiameter);
        }
    }
}
