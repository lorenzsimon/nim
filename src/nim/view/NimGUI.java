package nim.view;

import nim.model.Nim;
import nim.model.Misere;
import nim.model.Board;
import nim.model.Player;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 * The NimGUI class represents the graphical user interface for a nim game.
 * It displays suitable components for the visual representation and interaction
 * with the game.
 * <p>
 * The GUI consists of one frame and therefore extends the {@link JFrame} class.
 * <p>
 * The frame essentially consists of a service section that includes components
 * for general user control (control section) and information
 * (counter for selected sticks) and a game section that displays game specific
 * components (play board and row numbers).
 */
public class NimGUI extends JFrame {

    /**
     * The serial version UID of this class.
     */
    private static final long serialVersionUID = 561520581437659399L;

    /**
     * A panel that labels the rows of the sticks.
     */
    private JPanel rowNumbers;

    /**
     * A panel that displays the sticks on the play board.
     */
    private PlayBoard playBoard;

    /**
     * A button that enables the user to start a new game.
     */
    private JButton newGame;

    /**
     * A button that enables the user to undo the last move made by the user.
     */
    private JButton undoMove;

    /**
     * A button that enables the user to quit the program / close the frame.
     */
    private JButton quit;

    /**
     * A checkbox that enables the user to specify if the next game will be a
     * misere game.
     */
    private JCheckBox misereGame;

    /**
     * A checkbox that enables the user to specify if the machine should make
     * the first move in the next game.
     */
    private JCheckBox machineOpens;

    /**
     * A label that displays how many sticks are currently selected.
     */
    private JLabel selectedCounter;

    /**
     * The NewGameListener is a {@link ActionListener} that controls user input
     * suggesting that the user wants to start a new game.
     */
    private class NewGameListener implements ActionListener {

        /**
         * The default (changeable) input for the sticks of the new game.
         */
        private String defaultInput;

        /**
         * Handles the triggering user input (in form of a {@link ActionEvent})
         * by creating a new game.
         *
         * @param event The event that indicates that a specific action
         *              occurred.
         */
        @Override
        public void actionPerformed(ActionEvent event) {
            String input = showInputDialog("Enter your sticks.", defaultInput);

            // The input is null if the user clicks the cancel button.
            if (input == null) {
                return;
            }

            int[] parsedSticks;
            try {
                parsedSticks = parseSticks(input);
            } catch (NumberFormatException e) {
                showErrorMessage("Illegal input!");
                return;
            }

            /*
            The parsed sticks are null if the user entered rows with less that
            one stick in it.
             */
            if (parsedSticks != null) {

                /*
                The input is legal, so we want this input as default input when
                the action occurs next time.
                 */
                updateDefaultInput(parsedSticks);

                removeOldGameSection();
                addNewGameSection(parsedSticks);

                pack();
            }
        }

        /**
         * Parses the provided input into a numeric representation of the
         * sticks.
         * <p>
         * If the input contains numbers less than 1, it cannot get parsed for
         * further use and null gets returned because there is no appropriate
         * numeric representation of the provided "sticks".
         *
         * @param input The textual representation of the sticks.
         * @return The numeric representation of the entered sticks
         * (as an {@code int} array) or {@code null} if such representation
         * does not exist.
         */
        private int[] parseSticks(String input) {
            String[] tokens = input.trim().split("\\s+");

            int[] sticks = new int[tokens.length];

            for (int i = 0; i < tokens.length; i++) {
                int parsedToken = Integer.parseInt(tokens[i]);

                // The amount of sticks must be greater than 0 for each row.
                if (parsedToken < 1) {
                    showErrorMessage("Enter at least one stick per row!");
                    return null;
                }

                sticks[i] = parsedToken;
            }

            return sticks;
        }

        /**
         * Replaces the old default input value with the provided new one.
         * <p>
         * This method reverses {@link #parseSticks(String)} but is necessary in
         * order to remove potential spaces between the parameters entered by
         * the user.
         *
         * @param parameters The parameters that represent the new default input
         *                   value.
         */
        private void updateDefaultInput(int[] parameters) {
            StringBuilder builder = new StringBuilder();

            for (int i : parameters) {
                builder.append(i);
                builder.append(" ");
            }

            defaultInput = builder.toString();
        }

        /**
         * Removes the section that displayed the old game from the frame if
         * such panels currently exist in the frame.
         */
        private void removeOldGameSection() {
            if (playBoard != null) {
                undoMove.setEnabled(false);
                remove(playBoard);
            }

            if (rowNumbers != null) {
                remove(rowNumbers);
            }
        }

        /**
         * Adds a new game section specified by the provided sticks to the
         * frame.
         *
         * @param sticks The sticks that represent the initial state of the new
         *               game.
         */
        private void addNewGameSection(int[] sticks) {
            Board game = makeGame(sticks);

            playBoard = new PlayBoard(NimGUI.this, game, sticks.length,
                    getMaxSticks(sticks));
            add(playBoard, BorderLayout.CENTER);

            rowNumbers = new JPanel();
            rowNumbers.setLayout(new GridLayout(sticks.length, 1));
            for (int i = 1; i <= sticks.length; i++) {
                JLabel number = new JLabel(i + ":");
                number.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                rowNumbers.add(number);
            }
            add(rowNumbers, BorderLayout.WEST);
        }

        /**
         * Constructs a new game matching the provided {@code sticks} and the
         * current settings in the service section.
         *
         * @param sticks The initial sticks for the new game.
         * @return The proper game for the current settings.
         */
        private Board makeGame(int[] sticks) {
            Board game;

            if (machineOpens.isSelected()) {
                if (misereGame.isSelected()) {
                    game = new Misere(Player.MACHINE, sticks);

                    /*
                    The machine opens option is set, so the machine makes the
                    first move immediately.
                     */
                    game.machineRemove();
                    if (game.isGameOver()) {
                        showWinnerMessage(Player.HUMAN);
                    }
                } else {
                    game = new Nim(Player.MACHINE, sticks);

                    /*
                    The machine opens option is set, so the machine makes the
                    first move immediately.
                     */
                    game.machineRemove();
                    if (game.isGameOver()) {
                        showWinnerMessage(Player.MACHINE);
                    }
                }
            } else if (misereGame.isSelected()) {
                game = new Misere(Player.HUMAN, sticks);
            } else {
                game = new Nim(Player.HUMAN, sticks);
            }

            return game;
        }

        /**
         * Gets the amount of sticks in the row with the most sticks.
         *
         * @param sticks The sticks that are searched for the maximum.
         * @return The sticks that are the maximum of all rows.
         */
        private int getMaxSticks(int[] sticks) {
            int max = 0;

            for (int i : sticks) {
                if (i > max) {
                    max = i;
                }
            }

            return max;
        }
    }

    /**
     * Default constructor for a new Nim GUI with an initialized service section
     * and an empty game section.
     */
    public NimGUI() {
        super("Nim");

        JPanel serviceSection = new JPanel();
        initServiceSection(serviceSection);

        getContentPane().add(serviceSection, BorderLayout.EAST);

        addEventListeners();
    }

    /**
     * Initializes the service section (and its members) for a new GUI.
     *
     * @param serviceSection The service section to initialize.
     */
    private void initServiceSection(JPanel serviceSection) {
        serviceSection.setLayout(new BorderLayout());

        JPanel controlSection = new JPanel();
        initControlSection(controlSection);

        selectedCounter = new JLabel("", JLabel.CENTER);
        selectedCounter.setForeground(Color.BLUE);
        selectedCounter.setFont(new Font("Arial", Font.BOLD, 32));

        serviceSection.add(controlSection, BorderLayout.NORTH);
        serviceSection.add(selectedCounter, BorderLayout.SOUTH);
    }

    /**
     * Initializes the control section (and its members) for a new GUI.
     *
     * @param controlSection The control section to initialize.
     */
    private void initControlSection(JPanel controlSection) {
        controlSection.setLayout(new BoxLayout(controlSection,
                BoxLayout.Y_AXIS));

        initControlMembers();

        controlSection.add(newGame);
        controlSection.add(undoMove);
        controlSection.add(quit);
        controlSection.add(misereGame);
        controlSection.add(machineOpens);
    }

    /**
     * Initializes the members of the control section.
     */
    private void initControlMembers() {
        newGame = new JButton("New");
        newGame.setMnemonic(KeyEvent.VK_N);

        undoMove = new JButton("Undo");
        undoMove.setMnemonic(KeyEvent.VK_U);
        undoMove.setEnabled(false);

        quit = new JButton("Quit");
        quit.setMnemonic(KeyEvent.VK_Q);

        misereGame = new JCheckBox("Misere");
        misereGame.setMnemonic(KeyEvent.VK_M);

        machineOpens = new JCheckBox("Machine opens");
        machineOpens.setMnemonic(KeyEvent.VK_O);
    }

    /**
     * Adds event listeners to the relevant components of the frame.
     */
    private void addEventListeners() {
        newGame.addActionListener(new NewGameListener());

        undoMove.addActionListener(new ActionListener() {

            /**
             * Handles a click on the undo button by having the play board from
             * the previous stage of the game displayed.
             *
             * @param e The action event triggered by the click.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                playBoard.showPriorStage();
                undoMove.setEnabled(playBoard.priorStageAvailable());
            }
        });

        quit.addActionListener(new ActionListener() {

            /**
             * Handles a click on the quit button by closing the frame.
             *
             * @param e The action event triggered by the click.
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    /**
     * Enables the undo button.
     */
    public void enableUndo() {
        undoMove.setEnabled(true);
    }

    /**
     * Displays the provided number on the label for the counter of the selected
     * sticks.
     *
     * @param selected The text that indicates how many sticks are selected.
     */
    public void setSelectedCounter(int selected) {

        // We cannot select less than one stick.
        if (selected < 1) {
            selectedCounter.setText(null);
        } else {
            selectedCounter.setText(Integer.toString(selected));
        }
    }

    /**
     * Displays a message informing the user about who won the game.
     *
     * @param winner The winner of the game.
     */
    public void showWinnerMessage(Player winner) {
        if (winner == Player.HUMAN) {
            showMessageDialog(null, "You won!", "Congratulations!",
                    INFORMATION_MESSAGE);
        } else {
            showMessageDialog(null, "You lost!", "Sorry!", INFORMATION_MESSAGE);
        }
    }

    /**
     * Displays a message informing the user about an error.
     *
     * @param message The details of the error that occurred.
     */
    public void showErrorMessage(String message) {
        showMessageDialog(null, message, "Error!", ERROR_MESSAGE);
    }
}
