package nim.view;

import nim.model.Board;
import nim.model.Player;

import javax.swing.JPanel;

import java.awt.GridLayout;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import java.util.Stack;

/**
 * The play board is the visual representation of the board on which the sticks
 * that are not yet removed lie.
 * <p>
 * This class is therefor responsible for displaying the current game situation
 * for the user and provides the interaction interface for the game play.
 */
public class PlayBoard extends JPanel {

    /**
     * The serial version UID of this class.
     */
    private static final long serialVersionUID = 5019705099348766607L;

    /**
     * The gui to which this board belongs.
     */
    private NimGUI gui;

    /**
     * The game whose sticks this play board displays.
     */
    private Board game;

    /**
     * The stages of the game play. Every stage is a state of sticks before the
     * user makes a move.
     */
    private Stack<Board> gameStages;

    /**
     * The SlotMouseListener listens for mouse events occurring on the slots of
     * a play board.
     * <p>
     * This listener controls the user interaction with the active game.
     */
    private class SlotMouseListener extends MouseAdapter {

        /**
         * Handles a click on a slot of the board by performing a move specified
         * by the position of the slot on the board.
         * <p>
         * If the game is not over after the move is made, a machine move is
         * made automatically.
         * <p>
         * If the performed moves cause the end of the game, the user will be
         * notified who won the game.
         *
         * @param e The event that occurred on the slot.
         */
        @Override
        public void mouseClicked(MouseEvent e) {
            BoardSlot source = (BoardSlot) e.getSource();
            int row = source.getRow();
            int col = source.getCol();

            // We can only remove sticks from slots that are occupied.
            if (source.containsStick()) {

                /*
                Before the user clicked, he first entered the slot and selected
                sticks. We need to deselect these sticks and reset the selected
                counter of the view by simulating the exit of the mouse.
                 */
                mouseExited(e);

                // If the stack is empty, the undo button is currently disabled.
                if (gameStages.empty()) {

                    // Enable because we will add a new stage to the stack.
                    gui.enableUndo();
                }

                gameStages.push(game.clone());

                makeMove(row, col);

                if (!game.isGameOver()) {
                    makeMachineMove();
                }

                checkGameOver();
            }
        }

        /**
         * Handles the entering of the mouse cursor on a slot of the board by
         * having the board select the corresponding slots and having the gui
         * update the selected counter.
         *
         * @param e The event that occurred on the slot.
         */
        @Override
        public void mouseEntered(MouseEvent e) {
            BoardSlot source = (BoardSlot) e.getSource();
            int row = source.getRow();
            int col = source.getCol();

            // Empty slots cannot be selected.
            if (source.containsStick()) {
                updateSelectedSticks(row, col, true);
                gui.setSelectedCounter(game.getSticks(row) - col);
            }
        }

        /**
         * Handles the exiting of the mouse cursor from a slot by having the
         * board deselect the selected sticks and having the gui reset the
         * selected counter.
         *
         * @param e The event that occurred on the slot.
         */
        @Override
        public void mouseExited(MouseEvent e) {
            BoardSlot source = (BoardSlot) e.getSource();
            int row = source.getRow();
            int col = source.getCol();

            // Empty slots never got selected.
            if (source.containsStick()) {
                updateSelectedSticks(row, col, false);
                gui.setSelectedCounter(0);
            }
        }

        /**
         * Performs the necessary steps for a move (changes the model and
         * prompts the view to adjust to the updated model).
         *
         * @param row The row from which sticks are removed.
         * @param col The column from which all sticks that are in the same row
         *            and right of it (including this column) are removed.
         */
        private void makeMove(int row, int col) {
            int amount = game.getSticks(row) - col;

            game.remove(row, amount);

            updateRowSection(row, col);
        }

        /**
         * Performs the steps of a machine move similarly to
         * {@link #makeMove(int, int)}.
         */
        private void makeMachineMove() {
            game.machineRemove();

            int row = game.getLastMove().getRowOfSticks();
            int col = game.getSticks(row);

            updateRowSection(row, col);
        }

        /**
         * Checks if a game is over and if so prompts the gui to inform the user
         * about the winner.
         */
        private void checkGameOver() {
            if (game.isGameOver()) {
                Player winner = game.getWinner();
                gui.showWinnerMessage(winner);
            }
        }
    }

    /**
     * Constructor of a new play board that is part of the provided {@code gui},
     * displays the provided {@code game} and has {@code rows} X {@code cols}
     * slots.
     * <p>
     * Be aware that the rows and columns of the play board don't necessary have
     * to match the rows and columns of the provided game.
     * The game model gets passed as an argument to ensure, that all possible
     * kinds of boards can get displayed.
     *
     * @param gui  The gui of which the play board is part of.
     * @param game The game that the play board represents.
     * @param rows The amount of rows of the play board.
     * @param cols The amount of columns of the play board.
     */
    public PlayBoard(NimGUI gui, Board game, int rows, int cols) {
        setLayout(new GridLayout(rows, cols));

        this.gui = gui;
        this.game = game.clone();
        gameStages = new Stack<>();

        MouseListener listener = new SlotMouseListener();

        // Fill the board with stick slots.
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                BoardSlot slot = new BoardSlot(row, col, listener);

                if (col < game.getSticks(row)) {
                    slot.setContainsStick(true);
                }

                add(slot);
            }
        }
    }

    /**
     * Checks if the board can display a previous stage of the game.
     *
     * @return {@code True} if a previous stage can be displayed, {@code false}
     * if not.
     */
    public boolean priorStageAvailable() {
        return !gameStages.empty();
    }

    /**
     * Makes the play board display the previous game stage.
     */
    public void showPriorStage() {
        if (priorStageAvailable()) {
            game = gameStages.pop();
            updateBoard();
        }
    }

    /**
     * Updates the displayed sticks in the play board by adjusting to the
     * current game model.
     */
    private void updateBoard() {
        for (int i = 0; i < ((GridLayout) getLayout()).getRows(); i++) {
            updateRowSection(i, 0);
        }
    }

    /**
     * Updates which slots in a provided row section display a stick by
     * requesting the current stick state from the game model.
     *
     * @param row The row that needs to be updated.
     * @param col The column that defines the start of the section.
     */
    private void updateRowSection(int row, int col) {
        int colsInBoard = ((GridLayout) getLayout()).getColumns();
        int index = (colsInBoard * row) + col;

        for (int i = 0; i < colsInBoard - col; i++) {
            BoardSlot slot = (BoardSlot) getComponent(index + i);

            if ((col + i) < game.getSticks(row)) {
                slot.setContainsStick(true);
            } else {
                slot.setContainsStick(false);
            }
        }
    }

    /**
     * Updates which sticks in the a provided row are displayed as selected.
     * <p>
     * All sticks that are in the provided row and to the right of the provided
     * column (including this very column) are displayed as selected/deselected.
     *
     * @param row   The row in which sticks are selected/deselected.
     * @param col   The column that defines the start of the selection section.
     * @param value Flag that determines if the section gets selected
     *              (value = {@code true}) or deselected (value = {@code false})
     */
    private void updateSelectedSticks(int row, int col, boolean value) {
        int colsInBoard = ((GridLayout) getLayout()).getColumns();
        int index = (colsInBoard * row) + col;

        // Slots without sticks on it cannot be selected (minimizes section).
        for (int i = 0; i < game.getSticks(row) - col; i++) {
            BoardSlot slot = (BoardSlot) getComponent(index + i);
            slot.setIsSelected(value);
        }
    }
}
