package nim;

import nim.model.Misere;
import nim.model.Nim;
import nim.model.Board;
import nim.model.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * The Shell class manages the interaction between the user and the
 * Nim/Miser game.
 * It provides a user interface and ensures a legal game.
 *
 * @version 1.0
 */
public final class Shell {

    /** The player who makes the initial move of any new game. */
    private static Player startingPlayer = Player.HUMAN;

    /** The one and only game the user plays. */
    private static Board activeGame;

    /**
     * Constructs a new Shell object.
     */
    private Shell() {
    }

    /**
     * Represents the starting point of the program.
     *
     * @param args Optional arguments for the program start.
     * @throws IOException If a I/O error occurs.
     */
    public static void main(String[] args) throws IOException {
        InputStreamReader streamReader = new InputStreamReader(System.in);
        BufferedReader stdin = new BufferedReader(streamReader);

        execute(stdin);
    }

    /**
     * Checks and executes provided commands from the user.
     *
     * @param stdin The Reader object that provides the user input.
     * @throws IOException If a I/O error occurs.
     */
    private static void execute(BufferedReader stdin) throws IOException {
        System.out.println("The human makes the initial move "
                + "of the next new game.");
        boolean quit = false;

        while (!quit) {
            System.out.println("nim> ");
            String input = stdin.readLine();

            if (input == null) {
                break;
            }

            //Array containing all words, that were separated by spaces.
            String[] tokens = input.toLowerCase().trim().split("\\s+");

            //Check if there are any words in the tokens Array
            if (tokens[0].length() > 0) {
                char firstLetter = tokens[0].charAt(0);

                switch (firstLetter) {
                    case 'n':
                        newMethod(tokens, true);
                        break;
                    case 'm':
                        newMethod(tokens, false);
                        break;
                    case 'r':
                        removeMethod(tokens);
                        break;
                    case 's':
                        switchMethod();
                        break;
                    case 'p':
                        printMethod();
                        break;
                    case 'v':
                        verboseMethod(tokens);
                        break;
                    case 'h':
                        printHelpScreen();
                        break;
                    case 'q':
                        quit = true;
                        break;
                    default:
                        System.out.println(Nim.ERROR_MSG + tokens[0]
                                + " is not a valid command.");
                        break;
                }
            } else {
                System.out.println(Nim.ERROR_MSG
                        + "Enter a command.");
            }
        }
    }

    /**
     * Starts a new Nim or Misere game.
     *
     * @param tokens The user input.
     * @param nim    A boolean flag to differentiate between new Nim game and
     *               new Misere game.
     */
    private static void newMethod(String[] tokens, boolean nim) {

        //We need to have at least two parameters (at least one row).
        if (tokens.length > 1) {

            //Cut off the first parameter ("(N|n).*")
            String[] tempArray = Arrays.copyOfRange(tokens, 1, tokens.length);

            /*
            toIntArray() can throw a NumberFormatException during parsing or
            throw an IllegalArgumentException if a row has less than one stick.
             */
            try {
                int[] startingState = toIntArray(tempArray);

                if (nim) {
                    activeGame = new Nim(startingPlayer, startingState);
                } else {
                    activeGame = new Misere(startingPlayer, startingState);
                }

                //We have to make a machine move if the machine is starting.
                if (startingPlayer == Player.MACHINE) {
                    checkMachineRemove();
                }
            } catch (NumberFormatException n) {
                System.out.println(Nim.ERROR_MSG
                        + "Enter only whole numbers for sticks.");
            } catch (IllegalArgumentException i) {
                System.out.println(i.getMessage());
            }
        } else {
            System.out.println(Nim.ERROR_MSG
                    + "Enter at least one row.");
        }
    }

    /**
     * Checks and executes a move from the user input.
     *
     * @param tokens The user input.
     */
    private static void removeMethod(String[] tokens) {

        //A remove commands consists of (at least) 3 parameters.
        if (tokens.length > 2) {

            //We cannot make a move if there is no active game.
            if (activeGame != null && !activeGame.isGameOver()) {

                //Cut off the first parameter ("(R|r).*")
                String[] tempArray = Arrays.copyOfRange(tokens, 1,
                        tokens.length);

                /*
                toIntArray() can throw a NumberFormatException during parsing
                or throw a IllegalArgumentException if the provided move is
                illegal (Row smaller than 1 and/or remove less than 1 stick.)
                remove() checks if the move is legal as well.
                 */
                try {
                    int[] removeParameters = toIntArray(tempArray);

                    //We need to transform from 1-indexing to 0-indexing.
                    activeGame.remove((removeParameters[0] - 1),
                            removeParameters[1]);

                    checkGameOver();
                    checkMachineRemove();
                } catch (NumberFormatException n) {
                    System.out.println(Nim.ERROR_MSG
                            + "Enter only whole numbers for sticks.");
                } catch (IllegalArgumentException i) {
                    System.out.println(i.getMessage());
                }
            } else {
                System.out.println(Nim.ERROR_MSG
                        + "Start a new game first.");
            }
        } else {
            System.out.println(Nim.ERROR_MSG
                    + "The 'REMOVE' command needs 3 parameters.");
        }
    }

    /**
     * Switches the {@link #startingPlayer starting Player} of the next new
     * game and provides information to the user.
     */
    private static void switchMethod() {
        if (startingPlayer == Player.HUMAN) {
            startingPlayer = Player.MACHINE;
            System.out.println("The machine makes the initial move "
                    + "of the next new game.");
        } else {
            startingPlayer = Player.HUMAN;
            System.out.println("The human makes the initial move "
                    + "of the next new game.");
        }
    }

    /**
     * Prints the current state of the game to the screen.
     */
    private  static void printMethod() {

        //We cannot access any methods of the game if we have no active game.
        if (activeGame != null) {
            System.out.println(activeGame);
        } else {
            System.out.println(Nim.ERROR_MSG
                    + "Start a new game first.");
        }
    }

    /**
     * Sets the global verbose flag.
     *
     * @param tokens The user input.
     */
    private static void verboseMethod(String[] tokens) {

        //We need a second parameter for this command.
        if (tokens.length > 1) {
            switch (tokens[1]) {
                case "on":
                    Nim.setVerbose(true);
                    break;
                case "off":
                    Nim.setVerbose(false);
                    break;
                default:
                    System.out.println(Nim.ERROR_MSG
                            + "Enter 'ON' or 'OFF'.");
                    break;
            }
        } else {
            System.out.println(Nim.ERROR_MSG
                    + "Enter a second parameter.");
        }
    }

    /**
     * Outputs the help screen.
     */
    private static void printHelpScreen() {
        System.out.println("Willkommen im Nim-Spiel");
        System.out.println("Das Spiel unterstützt folgende Befehle: \n");
        System.out.println("NEW <s1> <s2> ... <sn>:");
        System.out.println(
                "Erstellt ein neues Spiel mit n Reihen und si Streichhölzern.\n"
                + "Geben Sie mindestens 1 für jede Reihe ein. \n");
        System.out.println("MISERE <s1> <s2> ... <sn>:");
        System.out.println("Analog zu NEW, aber im Misere-Modus. \n");
        System.out.println("REMOVE <r> <s>:");
        System.out.println("Entfernt s Streichhölzer aus Reihe r. \n");
        System.out.println("SWITCH:");
        System.out.println(
                "Wechselt den Spieleröffner für das nächste Spiel. \n");
        System.out.println("PRINT:");
        System.out.println("Gibt die aktuelle Belegung des Feldes aus. \n");
        System.out.println("VERBOSE <o>:");
        System.out.println(
                "Schaltet erweiterte Ausgabe ein (o=ON) oder aus (o=OFF). \n");
        System.out.println("QUIT:");
        System.out.println("Beedet das Programm. \n");
    }

    /**
     * Transforms a string Array containing numbers in an int Array.
     *
     * @param tokens The numbers in string representation.
     * @return The numbers in int representation.
     */
    private static int[] toIntArray(String[] tokens) {
        int[] tempArray = new int[tokens.length];

        for (int i = 0; i < tokens.length; i++) {
            int parsedToken = Integer.parseInt(tokens[i]);

            //For all usages the numbers need to be greater than 0.
            if (parsedToken < 1) {
                throw new IllegalArgumentException(Nim.ERROR_MSG
                        + "Enter at least 1 for each parameter.");
            }

            tempArray[i] = parsedToken;
        }

        return tempArray;
    }

    /**
     * Checks if a game is over and if so provides information about the winner.
     */
    private static void checkGameOver() {
        if (activeGame.isGameOver()) {
            if (activeGame.getWinner() == Player.HUMAN) {
                System.out.println("Congratulations! You won.");
            } else {
                System.out.println("Sorry! Machine wins.");
            }
        }
    }

    /**
     * Makes a machine move if the game is not over and informs the user about
     * the move the machine has made.
     */
    private static void checkMachineRemove() {
        if (activeGame != null && !activeGame.isGameOver()) {
            activeGame.machineRemove();
            System.out.println("Player machine removed "
                    + activeGame.getLastMove());

            checkGameOver();
        }
    }
}
