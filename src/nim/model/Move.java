package nim.model;

/**
 * The Move class represents a move made in the nim game.
 * It is only a wrapper class and holds a (row, sticks) tuple.
 *
 * @version 1.0
 */
public class Move implements Cloneable {

    /** The row number from which sticks got removed. */
    private int rowOfSticks;

    /** The amount of sticks that got removed. */
    private int numberOfSticks;

    /**
     * Constructs a new Move object and sets the provided values.
     *
     * @param row The row from which the sticks got removed.
     * @param s   The amount of sticks that got removed.
     */
    public Move(int row, int s) {
        this.rowOfSticks = row;
        this.numberOfSticks = s;
    }

    /**
     * Gets the row-number from which the sticks got removed in the move.
     *
     * @return The row-number of the move.
     */
    public int getRowOfSticks() {
        return rowOfSticks;
    }

    /**
     * Gets the amount of sticks that got removed in the move.
     *
     * @return The amount of sticks of the move.
     */
    public int getNumberOfSticks() {
        return numberOfSticks;
    }

    /**
     * Gets the string representation of the Move object.
     *
     * @return The string representation of the Move object.
     */
    @Override
    public String toString() {
        return numberOfSticks + " stick(s) from row " + (rowOfSticks + 1) + ".";
    }

    /**
     * Creates and returns a (deep) copy of this Move object.
     *
     * @return The clone of this Move object.
     * @throws CloneNotSupportedException The super class does not support
     *                                    cloning.
     */
    @Override
    public Move clone() throws CloneNotSupportedException {
        return (Move) super.clone();
    }
}
