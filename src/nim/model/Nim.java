package nim.model;

import java.util.Arrays;

/**
 * The Nim class handles the state and internal behavior of a nim game.
 * It implements the {@link nim.model.Board} interface and should be only used
 * through it.
 * All methods of this class use 0-indexing.
 *
 * @version 1.0
 */
public class Nim implements Board {

    /** Constant, that represents the begin of every error massage. */
    public static final String ERROR_MSG = "Error! ";

    /** Global flag, for extended print of the {@link #sticks state}. */
    private static boolean verbose = false;

    /** The Player who is allowed to make the next move. */
    private Player activePlayer;

    /** Represents the current state of the sticks (how many per row). */
    private int[] sticks;

    /** The last move made. */
    private Move lastMove;

    /** The nim-sum for the current {@link #sticks state}. */
    private int nimSum;

    /**
     * Constructs a new Nim-Game-Board and sets the nim-sum.
     *
     * @param startingPlayer The starting Player
     * @param startingSticks The amount of rows (index) and sticks (values)
     */
    public Nim(Player startingPlayer, int[] startingSticks) {
        this.activePlayer = startingPlayer;
        this.sticks = startingSticks.clone();
        this.nimSum = updateNimSum();
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public int getRowCount() {
        return sticks.length;
    }

    /**
     * Gets the number of sticks currently in the row {@code row}.
     *
     * @param row The number of the zero indexed row ascending top down.
     * @return The number fo sticks in the row {@code}.
     * @throws IllegalArgumentException The {@code row} does not exist.
     */
    @Override
    public int getSticks(int row) {

        //Because it is a public method, we have to make sure, the row exists.
        if (row >= 0 && row < sticks.length) {
            return sticks[row];
        } else {
            throw new IllegalArgumentException(ERROR_MSG
                    + "Row number must be between 1 and "
                    + sticks.length + ".");
        }
    }

    /**
     * Executes a human move.
     *
     * @param row The number of the zero indexed row ascending top down.
     * @param s   The number of sticks to remove from row {@code row}. Must be
     *            at least 1 and at most the number of sticks of the row.
     * @throws IllegalStateException The game is over.
     * @throws IllegalStateException It's not the human's turn.
     */
    @Override
    public void remove(int row, int s) {

        //We cannot make a move if the game is over.
        if (isGameOver()) {
            throw new IllegalStateException(ERROR_MSG + "Game is over.");
        }

        //We can only make this move if it is the human's turn.
        if (activePlayer == Player.HUMAN) {

            /*
            This comment is only relevant for grading:

            If the provided move is illegal, makeMove() throws a
            IllegalArgumentException. We don't catch that here, so remove()
            also throws that exception and complies with the interface.
            I don't mention unchecked exceptions in the JavaDoc if they are not
            thrown directly in that method.
             */
            makeMove(row, s);
        } else {
            throw new IllegalStateException(ERROR_MSG
                    + "It's not the human's turn!");
        }
    }

    /**
     * Executes a machine move.
     *
     * @throws IllegalStateException The game is over.
     * @throws IllegalStateException It's not the machine's turn.
     */
    @Override
    public void machineRemove() {

        //We cannot make a move if the game is over.
        if (isGameOver()) {
            throw new IllegalStateException(ERROR_MSG + "Game is over.");
        }

        //We can only make this move if it is the machine's turn.
        if (activePlayer == Player.MACHINE) {

            //Make a specific move in case of a safe combination.
            if (nimSum == 0) {
                makeStupidMachineMove();
            } else {
                int row = chooseRow();

                //nimSum ^ sticks[row] gives us the new sticks[row].
                int amount = sticks[row] - (nimSum ^ sticks[row]);

                makeMove(row, amount);
            }
        } else {
            throw new IllegalStateException(ERROR_MSG
                    + "It's not the machine's turn!");
        }
    }

    /**
     * Gets the last move made in the game.
     *
     * @return The last move made as an object Move (number of sticks and the
     * 0-indexed row from which they were removed).
     * @throws IllegalStateException No move has been made yet.
     */
    @Override
    public Move getLastMove() {

        //We have to first check if a move was made.
        if (lastMove == null) {
            throw new IllegalStateException(ERROR_MSG
                    + "No move has been made yet.");
        } else {
            return lastMove;
        }
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public boolean isGameOver() {

        //The game is over when the sum of all sticks is 0.
        return Arrays.stream(sticks).sum() == 0;
    }

    /**
     * Checks and returns the player who has won the game.
     *
     * @return The Player who took the last stick.
     * @throws IllegalStateException The Player is not set.
     *                               (Not possible with only that constructor)
     * @throws IllegalStateException The game is not over.
     */
    @Override
    public Player getWinner() {
        
        //There can only be a winner if the game is over.
        if (isGameOver()) {
            if (activePlayer == Player.HUMAN) {
                return Player.MACHINE;
            } else if (activePlayer == Player.MACHINE) {
                return Player.HUMAN;
            } else {
                throw new IllegalStateException(ERROR_MSG
                        + "No active player is set.");
            }
        } else {
            throw new IllegalStateException(ERROR_MSG
                    + "Game is not over yet.");
        }
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public Board clone() {
        Nim copy = null;

        /*
        Try to (shallow) copy Nim, Array and Move object.
        Can throw an exception.
         */
        try {
            copy = (Nim) super.clone();
            copy.sticks = this.sticks.clone();

            //Prevent a NullPointerException.
            if (this.lastMove != null) {
                copy.lastMove = this.lastMove.clone();
            }
        } catch (CloneNotSupportedException e) {
            System.out.println(ERROR_MSG + e.getMessage());
        }

        return copy;
    }

    /**
     *{@inheritDoc}
     */
    @Override
    public String toString() {
        StringBuilder tempString = new StringBuilder();

        for (int i = 0; i < sticks.length; i++) {
            tempString.append((i + 1) + ": " + sticks[i]);

            //Binary representation only gets added if verbose flag is set.
            if (verbose) {
                tempString.append(" (" + Integer.toBinaryString(sticks[i])
                        + ")");
            }

            //No line breaks are inserted after the last row.
            if ((i + 1) < sticks.length) {
                tempString.append("\n");
            }
        }

        //This additional line only gets added if verbose flag is set.
        if (verbose) {

            //The nim sum gets added in a new line.
            tempString.append("\n" + "Nim sum: " + nimSum + " ("
                    + Integer.toBinaryString(nimSum) + ")");
        }

        return tempString.toString();
    }

    /**
     * Sets the verbose flag on {@code value}.
     *
     * @param value The value for the {@link #verbose flag}.
     */
    public static void setVerbose(boolean value) {
        verbose = value;
    }

    /**
     * Gets the {@link #activePlayer active Player} of the game.
     * It is package-private for the {@link Misere} class.
     *
     * @return The {@link #activePlayer active Player}.
     */
    Player getActivePlayer() {
        return this.activePlayer;
    }

    /**
     * Performs the steps of a move.
     * It is package-private for the {@link Misere} class.
     *
     * @param row The row from which the sticks get removed.
     * @param s   The amount of sticks to be removed.
     * @throws IllegalArgumentException Provided row number is illegal.
     * @throws IllegalArgumentException Provided stick number is illegal.
     */
    void makeMove(int row, int s) {
        
        /*
        We cannot make that move if the provided row- or/and stick-number 
        is illegal.
         */
        if (row < 0 || row >= sticks.length) {
            throw new IllegalArgumentException(ERROR_MSG
                    + "Row number must be between 1 and "
                    + (sticks.length) + ".");
        } else if (s < 1 || s > sticks[row]) {
            throw new IllegalArgumentException(ERROR_MSG
                    + "Number of sticks must be between 1 and "
                    + sticks[row] + ".");
        } else {
            sticks[row] = sticks[row] - s;
        }

        lastMove = new Move(row, s);
        updateNimSum();
        swapPlayer();
    }

    /**
     * Updates the nim-sum for the current {@link #sticks state}.
     *
     * @return The correct nim-sum for the current {@link #sticks state}.
     */
    private int updateNimSum() {
        int tempNimSum = sticks[0];

        /*
        For each row (indexes in Array sticks) the temporary nim-sum gets
        "XOR-linked" to the previous temporary nim-sum (pairwise linking).
         */
        for (int i = 1; i < sticks.length; i++) {
            tempNimSum = tempNimSum ^ sticks[i];
        }

        nimSum = tempNimSum;
        return nimSum;
    }

    /**
     * Finds a row where the highest bit set of the nim-sum is also set.
     *
     * @return The "smallest" row-number where the highest bit is set.
     * @throws IllegalStateException There is no convenient row.
     */
    private int chooseRow() {
        for (int i = 0; i < sticks.length; i++) {
            if (bitIsSet(highestBitSet(nimSum), sticks[i])) {
                
                //We return the first row, the bit is set.
                return i;
            }
        }

        /*
        This should not be possible, because we only use this method if 
        the nim-sum is not 0.
         */
        throw new IllegalStateException(ERROR_MSG
                + "No matching row available.");
    }

    /**
     * Calculates the position (0-indexed) of the highest bit set in a given
     * parameter.
     *
     * @param number The number from which the highest bit set gets calculated.
     * @return The position of the highest bit set or 0 if {@code number} is 0.
     */
    private int highestBitSet(int number) {
        int counter = 0;

        /*
        The number gets bitwise shifted until the highest bit is at position 0.
        The counter counts the position of the remaining 1.
         */
        while (number > 1) {
            number = number >> 1;
            counter = counter + 1;
        }

        return counter;
    }

    /**
     * Changes the Player who is allowed to make the next move.
     */
    private void swapPlayer() {
        if (activePlayer == Player.HUMAN) {
            activePlayer = Player.MACHINE;
        } else {
            activePlayer = Player.HUMAN;
        }
    }

    /**
     * Checks if a Bit is set in a number.
     *
     * @param index  The 0-indexed position of the bit.
     * @param number The number which gets checked.
     * @return True if the bit is set, false otherwise.
     */
    private boolean bitIsSet(int index, int number) {

        /*
        If 1 gets shifted "index" times to the left, we get a number where only
        the bit at position "index" is set.
        After the logical AND operation, we can only not get 0 if the number
        has this bit set as well.
         */
        return ((1 << index) & number) != 0;
    }

    /**
     * Executes the steps of a machine move, in case of a safe combination.
     */
    private void makeStupidMachineMove() {
        boolean rowFound = false;

        //Is a counter variable.
        int row = 0;

        while (!rowFound) {
            
            //We are looking for a row that is not empty.
            if (sticks[row] > 0) {
                rowFound = true;
            } else {
                row++;
            }
        }

        int removeSticks = roundUpHalfInt(sticks[row]);
        makeMove(row, removeSticks);
    }

    /**
     * Divides a {@code number} in half and rounds it up.
     *
     * @param number The number which gets divided and rounded up.
     * @return The divided and rounded number.
     */
    private int roundUpHalfInt(int number) {

        //We need a double for division.
        double tempResult = (double) number;

        tempResult = tempResult / 2.0;

        //Math.ceil rounds tempResult up.
        tempResult = Math.ceil(tempResult);

        return (int) tempResult;
    }
}
