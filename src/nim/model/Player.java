package nim.model;

/**
 * The Player enum represents the two possible instances of nim-players.
 * That means a player in the nim-game can either be a HUMAN or a MACHINE.
 *
 * @version 1.0
 */
public enum Player {

    /** Represents a human player. */
    HUMAN,

    /** Represents a machine player. */
    MACHINE
}
