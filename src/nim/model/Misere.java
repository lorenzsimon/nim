package nim.model;

/**
 * The Misere class represents a version of the Nim game.
 * Like the {@link Nim} class it handles the internal state and
 * behavior of a nim game, but behaves differently regarding the win strategy.
 * The Misere class extends the {@link Nim} class. So the Misere game
 * is a Nim game and can be used like one.
 * All methods of this class use 0-indexing.
 *
 * @version 1.0
 */
public class Misere extends Nim {

    /**
     * Constructs a new Misere-Game-Board and sets the nim-sum by using the Nim
     * constructor.
     *
     * @param startingPlayer The starting Player.
     * @param startingSticks The amount of rows (index) and sticks (values).
     */
    public Misere(Player startingPlayer, int[] startingSticks) {
        super(startingPlayer, startingSticks);
    }

    /**
     * Executes a machine move in a Misere game.
     *
     * @throws IllegalStateException It's not the machine's turn.
     */
    @Override
    public void machineRemove() {

        //We cannot make a move if it is not the machine's turn.
        if (getActivePlayer() != Player.MACHINE) {
            throw new IllegalStateException(Nim.ERROR_MSG
                    + "It's not the machine's turn!");
        }

        /*
        Make a specific move if there is only one row with more than one
        sticks left.
        Otherwise we make a normal move as we would in the Nim class.
         */
        if (getRowsBiggerThan(1) == 1) {

            /*
            If there is an even amount of not empty rows left, we remove all
            sticks from the row with the most sticks.
            Otherwise we remove all sticks except one from the row with the
            most sticks.
             */
            if ((getRowsBiggerThan(0) % 2) == 0) {
                int removeRow = getBiggestRow();
                int removeSticks = getSticks(removeRow);

                makeMove(removeRow, removeSticks);
            } else {
                int removeRow = getBiggestRow();
                int removeSticks = (getSticks(removeRow) - 1);

                makeMove(removeRow, removeSticks);
            }
        } else {
            super.machineRemove();
        }
    }

    /**
     * Checks and returns the Player who has won the game.
     *
     * @return The Player who did not take the last stick.
     */
    @Override
    public Player getWinner() {
        Player looser = super.getWinner();

        //The looser of a Nim game is the winner of a Misere game.
        if (looser == Player.HUMAN) {
            return Player.MACHINE;
        } else {
            return Player.HUMAN;
        }
    }

    /**
     * Gets the amount of rows that contain more than {@code number} sticks
     * for the current state.
     *
     * @param number The number of sticks each row gets checked for.
     * @return The amount of rows that have more than {@code number} sticks.
     */
    private int getRowsBiggerThan(int number) {
        int counter = 0;

        for (int i = 0; i < getRowCount(); i++) {
            if (getSticks(i) > number) {
                counter++;
            }
        }

        return counter;
    }

    /**
     * Gets the row with the most sticks in it.
     *
     * @return The row number with the most sticks in it.
     */
    private int getBiggestRow() {

        //Represents the row number with the most sticks in the range of [0-i].
        int tempRow = 0;

        for (int i = 0; i < getRowCount(); i++) {
            if (getSticks(i) > getSticks(tempRow)) {
                tempRow = i;
            }
        }

        return tempRow;
    }
}
